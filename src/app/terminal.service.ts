import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { webSocket, WebSocketSubjectConfig } from 'rxjs/webSocket';

@Injectable({
    providedIn: 'root',
})
export class TerminalService {
    public messages;
    private url: string = environment.BE_URL;

    constructor() {
        this.messages = webSocket(this.url);
    }
}
