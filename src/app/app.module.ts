import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { NgTerminalModule } from 'ng-terminal';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { TerminalComponent } from './terminal/terminal.component';

import { TerminalService } from './terminal.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from './material-module';
import { HeaderComponent } from './header/header.component';
import { HeaderCardComponent } from './header-card/header-card.component';
import { BuildCardComponent } from './build-card/build-card.component';

@NgModule({
    declarations: [AppComponent, ToolbarComponent, TerminalComponent, HeaderComponent, HeaderCardComponent, BuildCardComponent],
    imports: [BrowserModule, NgTerminalModule, BrowserAnimationsModule, MaterialModule],
    providers: [TerminalService],
    bootstrap: [AppComponent],
})
export class AppModule {}
