export default interface Message {
    author: string;
    type: string;
    message: string;
}
