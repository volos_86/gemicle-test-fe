import { Component } from '@angular/core';

import Card from '../interfaces/Card';
import Message from '../interfaces/Message';

import { TerminalService } from '../terminal.service';

@Component({
    selector: 'app-toolbar',
    templateUrl: './toolbar.component.html',
    styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent {
    cards: Card[] = [];

    constructor(private terminalService: TerminalService) {
        terminalService.messages.asObservable().subscribe(
            (msg: Message) => {
                if (msg.type === 'step') {
                    this.addCard(JSON.parse(msg.message));
                } else if (msg.type === 'startMsg') {
                    const res = JSON.parse(msg.message);
                    res.map((name) =>
                        this.cards.push({
                            name: name,
                            status: "haven't started yet",
                            creationtimestamp: null,
                            finishtimestamp: null,
                        })
                    );
                }
            },
            (err: Message) => console.log(`${err.message}`),
            () => console.log('complete')
        );
    }

    private addCard(card: Card): void {
        this.cards = this.cards.map((item) => {
            if (item.name === card.name) {
                return { ...item, ...card };
            } else {
                return item;
            }
        });
    }
}
