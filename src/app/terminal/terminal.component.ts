import { Component, ViewChild } from '@angular/core';
import { NgTerminal } from 'ng-terminal';

import { TerminalService } from '../terminal.service';
import Message from '../interfaces/Message';

@Component({
    selector: 'app-terminal',
    templateUrl: './terminal.component.html',
    styleUrls: ['./terminal.component.scss'],
})
export class TerminalComponent {
    disabled: boolean = false;
    private startBuildMessage: Message = {
        author: 'VOLOS',
        message: 'build',
        type: 'build',
    };

    @ViewChild('term', { static: true }) child: NgTerminal;

    constructor(private terminalService: TerminalService) {
        terminalService.messages.asObservable().subscribe((msg: Message) => this.child.write(`${msg.message}`));
    }

    sendMsg(): void {
        this.disabled = !this.disabled;
        this.terminalService.messages.next(this.startBuildMessage);
    }
}
