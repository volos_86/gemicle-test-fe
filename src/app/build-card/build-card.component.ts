import { Component, Input } from '@angular/core';

@Component({
    selector: 'app-build-card',
    templateUrl: './build-card.component.html',
    styleUrls: ['./build-card.component.scss'],
})
export class BuildCardComponent {
    @Input() name = '';
    @Input() creationtimestamp = 0;
    @Input() finishtimestamp = 0;
    @Input() statusstep = '';

    constructor() {}
}
